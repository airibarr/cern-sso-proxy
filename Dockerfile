FROM cern/cc7-base

RUN yum update -y && yum install -y httpd shibboleth log4shib xmltooling-schemas opensaml-schemas libcurl-openssl mod_auth_kerb mod_ldap mod_ssl gettext && \
    yum clean all

# Shibboleth cannot use system libcurl on centos cf. https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPLinuxRH6
ENV LD_PRELOAD=/opt/shibboleth/lib64/libcurl.so.4

# Prepare folders and make sure permissions allow running httpd/shibd as random unprivileged user ID.
# /var/rub/shibboleth will be an EmptyDir where both containers can write
RUN mkdir -p /usr/local/httpd/ && chmod a+rw /usr/local/httpd && \
    mkdir -p /var/run/shibboleth/ && chmod a+rw /var/run/shibboleth && \
    mkdir -p /etc/httpd/conf.d/configurable/ && chmod -R a+rw /etc/httpd/conf.d/ && chmod a+rw /etc/httpd/conf/httpd.conf && \
    chmod a+rw /etc/shibboleth/ && touch /etc/shibboleth/shibboleth2.xml && chmod a+rw /etc/shibboleth/shibboleth2.xml && \
    mkdir -p /run/httpd/ && chown -R root:root /run/httpd && chmod a+rw /run/httpd

# Download Shibboleth config files from linuxsoft servers (not shibboleth2.xml as we manually modify it)
RUN curl -o /etc/shibboleth/ADFS-metadata.xml http://linux.web.cern.ch/linux/scientific6/docs/shibboleth/ADFS-metadata.xml && \
    curl -o /etc/shibboleth/attribute-map.xml http://linux.web.cern.ch/linux/scientific6/docs/shibboleth/attribute-map.xml && \
    curl -o /etc/shibboleth/wsignout.gif http://linux.web.cern.ch/linux/scientific6/docs/shibboleth/wsignout.gif

COPY shib_httpd.conf /etc/shibboleth/
COPY httpd.conf /etc/httpd/conf/httpd.conf

# Copy entrypoint files, sibboleth config template and redirect shibboleth logs to stdout
COPY httpd.sh shib.sh shibboleth2.xml /
RUN chmod +x /httpd.sh && chmod +x /shib.sh && chmod a+rw /shibboleth2.xml && \
    chown root:root /var/log/shibboleth && chmod -R a+rw /var/log/shibboleth && ln -sf /dev/stdout /var/log/shibboleth/shibd.log

# entrypoint will be overriden for the shibboleth container
ENTRYPOINT ["/httpd.sh"]
EXPOSE 8081
