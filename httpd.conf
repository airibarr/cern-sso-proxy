# https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPApacheConfig

# Create only one child httpd process on startup. By default, the prefork MPM is used
# (ref. https://httpd.apache.org/docs/2.4/mod/prefork.html), which is non-threaded and deals
# with simultaneos requests by spawning child processes.
# With this configuration below, the proxy supports a maximum of 16 simultaneos requests
# (which will translate into 16 subprocesses) and the server will kill spare connections until
# there is only one left. These values can be overriden via a file in the ConfigMap.

StartServers 1
MinSpareServers 1
# MaxSpareServers has to be at least `MinSpareServers` + 1
# https://httpd.apache.org/docs/2.4/mod/prefork.html#maxspareservers
MaxSpareServers 2
MaxRequestWorkers 16

# allow large HTTP headers: users members of many groups can have a very large ADFS_GROUP
LimitRequestFieldsize 65536

Include conf.modules.d/*.conf
# Load the Shibboleth module.
LoadModule mod_shib /usr/lib64/shibboleth/mod_shib_24.so

Include /etc/shibboleth/shib_httpd.conf
Include conf.d/configurable/*.conf
# in particular do not include mod_ssl by default, neither mod_auth_kerb which prevents passing REMOTE_USER to backend
TypesConfig /etc/mime.types

PidFile /usr/local/httpd/httpd.pid
Listen 8081
ErrorLog /dev/stderr
TransferLog /dev/stdout
ServerName https://${HOSTNAME_FQDN}
